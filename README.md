# Video Resolution Upscaling Using Neural Networks

Vezměte krátké 5-10 sekundové video a vytvořte generátor, který bude generovat video s vyšším rozlišením.


https://arxiv.org/abs/1609.04802




## Implementace v TF s pouzitim jednoducheho MSE

https://gitlab.fit.cvut.cz/nankaond/mvi-sp/blob/master/SRGAN.ipynb

### Data

Stažení a zpracování dat je zakomentované v samotném notebooku, skript je připraven na načtení z dat Google Drive.

https://data.vision.ee.ethz.ch/cvl/DIV2K/

## Implementace v TF Keras s pouzitim site VGG pro lepsi loss funkci 

https://gitlab.fit.cvut.cz/nankaond/mvi-sp/blob/master/SRGAN-2ndTRY.ipynb


### Data


Stažení a zpracování dat je zakomentované v samotném notebooku, skript je připraven na načtení z dat Google Drive.

http://cocodataset.org/#home

## Výsledky

Průběžné výsledky z průběhu tréningu - checkpointy, váhy modelu, nahodne upscalovane framy pro každou pátou epochu tréningu.

https://drive.google.com/open?id=1-6lKhG_h4-hNWmey6mhxzlQmfkICLVbD

Notebook implementujici rozlozeni videa na jednotlive frames, jejich oriznuti na potrebnou velikost a bikubicky downscaling a nasledny upscaling pomoci srgan s pouzitim vgg a opetovne slozeni do videa pomoci ffmpeg.

https://gitlab.fit.cvut.cz/nankaond/mvi-sp/blob/master/video.ipynb

Výsledky doposud natrénovaného modelu na videu


[Sea dog and ball](https://www.youtube.com/watch?v=lvwLhzCRBEM&feature=youtu.be)
[Sheeps chewing](https://www.youtube.com/watch?v=wRigzcSHP0M&feature=youtu.be)
[Jellyfish](https://www.youtube.com/watch?v=r_LydST1hik&feature=youtu.be)
[Dog](https://www.youtube.com/watch?v=w7kJpZUnkI8&feature=youtu.be)
[Psychedelic face](https://www.youtube.com/watch?v=yEZVtZFvirU&feature=youtu.be)
[Fractal zoom](https://www.youtube.com/watch?v=Jooi5cuuFXo&feature=youtu.be)